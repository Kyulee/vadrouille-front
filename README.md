<img src="./assets/logo.png" width="200" height="100" alt="VadrouilleLogo"/>

<img src="https://safaridesmetiers.tech/wp-content/uploads/2021/08/MyDigitalSchool.png" width="200" height="100" alt="MDSLogo"/>

# La Vadrouille - Application Client

### Contexte du projet
Dans le cadre de la validation d'un MBA Développeur web full-stack, un projet de start-up doit être simuler.
Le sujet choisi est par l'équipe de 5 personnes constitué d'une développeuse web , de deux UI/UX designer, de 3 marketeuses digitale.

Ce projet a été réalisé durant l'année 2020 jusqu'en 2022.

#### Comment lutter contre l'isolement des personnes âgées et comment les rendre plus indépendants face au digital ? ####

Pour répondre à cette problématique, nous avons pour projet de créer une application destinée aux personnes âgées : 

### La Vadrouille

Il s'agit d'une application à laquelle ils auront accès depuis leur smartphone ou depuis leur tablette. Le principe est simple : on s'inscrit, et on choisit une activité directement sur l'application. Toutes les activités proposées sont adaptées aux personnes âgées, en réservant et payant via l'application, ils bénéficient d'un tarif promotionnel. 

De notre côté, nous mettons en place des partenariats avec les différents acteurs de chaque région pour proposer un panel d'activité complet qui correspond à tous. Plusieurs catégories seront disponibles, avec par exemple des activités sportives, culturelles, zen ou encore gourmandes.

## Visuels
Mettre des visuels de La Vadrouille

## Technologies utilisées

Expo v.43.0.0

React Native v.0.64.3

AWS Amplify v.4.3.17

React Native Elements v.3.4.2

React Navigation v.6.0.6

Stripe v.0.13.1
 

## Version
MVP

## Comment installer La Vadrouille ?

Cloner le repository via votre terminal

```
git clone https://gitlab.com/Kyulee/vadrouille-front.git
```

Installer le projet
```
npm install
```

Lancer le projet
```
expo start
```

## Contributeurs

Développeuse : NGUYEN Julie as Kyulee

UI/UX Designer : BOIS Camille 

UI/UX Designer : DA SANTA Lucie

Marketeuse : RASOLO Andréa

Marketeuse : NGUYEN Adeline

Marketeuse/Chef de projet : COULIBALY Ines


## Accès au projet en ligne

[Vadrouille projet client lien web](https://main.d1zmfuf0lugxg4.amplifyapp.com)

## Support
Pour toutes informations supplémentaires, vous pouvez envoyer vos questions à Kyulee.
 


