import React, { useEffect, useState } from "react";
import { Text, Image, View } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import { Icon } from "react-native-elements";

import { ScrollView } from "react-native-gesture-handler";
import CardActivite from "../../components/CardActivite";
import ButtonCategory from "../../components/ButtonCategory";
import uriDataCategory from "../../services/uriDataCategory";
import { env } from "../../../environment";

const HomeScreen = () => {
  const [categories, setCategories] = useState([]);

  useEffect(async () => {
    const res = await fetch(`${env.apiUrl}/categories`);
    const json = await res.json();
    setCategories(json.Items);
  }, []);

  return (
    <SafeAreaView>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
        }}
      >
        <Image
          source={require("../../../assets/logo.png")}
          style={{
            width: 90,
            height: 50,
            marginTop: 10,
            marginLeft: 10,
            resizeMode: "stretch",
          }}
        />
        <View style={{ flexDirection: "row", marginTop: 10 }}>
          <Icon type="ionicon" name="location-outline" color="#01B091" />
          <Text
            style={{
              fontSize: 20,
              marginRight: 10,
              textDecorationLine: "underline",
              color: "#01B091",
            }}
          >
            92320
          </Text>
        </View>
      </View>
      <ScrollView>
        <Text style={{ fontSize: 25, marginBottom: 10, padding: 10 }}>
          Catégories
        </Text>
        <ScrollView horizontal>
          {categories.map((category) => (
            <ButtonCategory
              category={category}
              key={category.id}
              imageUri={uriDataCategory.find((u) => u.id === category.id).uri}
            />
          ))}
        </ScrollView>
        <Text style={{ fontSize: 25, marginBottom: 10, padding: 10 }}>
          Activités à proximité
        </Text>
        <CardActivite />
      </ScrollView>
    </SafeAreaView>
  );
};

export default HomeScreen;
