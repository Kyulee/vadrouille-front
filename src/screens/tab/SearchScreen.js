import React, { useState, useEffect } from "react";
import { Text, Button } from "react-native";
import { Slider, Icon, SearchBar } from "react-native-elements";
import { ScrollView } from "react-native-gesture-handler";
import { SafeAreaView } from "react-native-safe-area-context";
import { env } from "../../../environment";
import CardActivite from "../../components/CardActivite";

const SearchScreen = () => {
  // const [distance, setDistance] = useState(0);
  const [query, setQuery] = useState("");
  //const [search, setSearch] = useState([]);

  // useEffect(async () => {
  //   const res = await fetch(
  //     `${env.apiUrl}/activity/search?activityName=${query}`,
  //     { method: "POST" }
  //   );
  //   const json = await res.json();
  //   setSearch(json);
  // }, [query]);
  return (
    <SafeAreaView style={{ marginBottom: 70 }}>
      <Text style={{ fontSize: 25, marginBottom: 10, padding: 10 }}>
        Rechercher une activité
      </Text>
      <SearchBar
        placeholder="ex : Peinture, pétanque, randonnée ..."
        lightTheme
        onChangeText={(value) => setQuery(value)}
        value={query}
      />
      {/* <Text
        style={{
          fontSize: 20,
          marginBottom: 10,
          padding: 10,
          color: "#01B091",
        }}
      >
        Recommandations
      </Text> */}
      <ScrollView style={{ marginTop: 20 }}>
        {/* {topics.map((topic) => {
          return (
            <View
              style={{
                backgroundColor: "#01B091",
                padding: 10,
                borderRadius: 10,
                margin: 10,
                width: 120,

                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <TouchableOpacity activeOpacity={1} index={topic.id}>
                <View />
                <Text style={{ color: "white" }}>{topic.text}</Text>
              </TouchableOpacity>
            </View>
          );
        })} */}
        <CardActivite search={query} />
      </ScrollView>
      {/* <Text
        style={{
          fontSize: 20,
          marginBottom: 10,
          padding: 10,
          color: "#01B091",
        }}
      >
        Distance
      </Text> 
      <View
        style={{
          width: 350,
          alignSelf: "center",
        }}
      >
        <Slider
          value={distance}
          onValueChange={(value) => setDistance(value)}
          step={5}
          maximumValue={100}
          trackStyle={{ height: 10, backgroundColor: "transparent" }}
          thumbStyle={{ height: 20, width: 20, backgroundColor: "transparent" }}
          thumbProps={{
            children: (
              <Icon
                name="walk-outline"
                type="ionicon"
                reverse
                containerStyle={{ bottom: 20, right: 20 }}
                color="#f398bb"
              />
            ),
          }}
        />
      </View>
      <Text style={{ marginTop: 20, textAlign: "center" }}>
        Je peux me déplacer jusqu'a {distance} km.
      </Text>*/}
    </SafeAreaView>
  );
};

export default SearchScreen;
