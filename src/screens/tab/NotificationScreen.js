import React from "react";
import { View, Text } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import { ListItem, Avatar } from "react-native-elements";
import mockDataNotif from "../../services/mockDataNotif.json";

const NotificationScreen = () => {
  const list = mockDataNotif;
  return (
    <SafeAreaView>
      <Text style={{ fontSize: 25, marginBottom: 10, padding: 10 }}>
        Notifications
      </Text>

      <View>
        {list.map((l, i) => (
          <ListItem key={i} bottomDivider>
            <Avatar source={{ uri: l.avatar_url }} />
            <ListItem.Content>
              <ListItem.Title>{l.title}</ListItem.Title>
              <ListItem.Subtitle>{l.description}</ListItem.Subtitle>
            </ListItem.Content>
          </ListItem>
        ))}
      </View>
    </SafeAreaView>
  );
};

export default NotificationScreen;
