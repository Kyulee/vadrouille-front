import React, { useState } from "react";
import { useFocusEffect } from "@react-navigation/native";
import { Text, Image } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import { ScrollView } from "react-native-gesture-handler";
import storage from "../../services/storage";
import { Activite } from "../../components/CardActivite";

const LikeScreen = () => {
  const [favorites, setFavorites] = useState([]);

  useFocusEffect(
    React.useCallback(() => {
      (async () => {
        const res = await storage.getAllDataForKey("like");
        setFavorites(res);
      })();
    }, [])
  );

  return (
    <SafeAreaView>
      <Image
        source={require("../../../assets/logo.png")}
        style={{
          width: 90,
          height: 50,
          marginTop: 10,
          marginLeft: 10,
          resizeMode: "stretch",
        }}
      />
      <Text style={{ fontSize: 25, marginBottom: 10, padding: 10 }}>
        Mes Favoris
      </Text>
      <ScrollView style={{ marginBottom: 60 }}>
        {favorites.map((activity) => (
          <Activite
            key={activity.id}
            activite={activity}
            isLiked={activity.isLiked}
          />
        ))}
        {!favorites.length && (
          <>
            <Image
              source={{
                uri: "https://cdn-icons-png.flaticon.com/512/594/594848.png?w=1380",
              }}
              style={{
                width: 50,
                height: 50,
                alignSelf: "center",
                resizeMode: "stretch",
              }}
            />
            <Text style={{ fontSize: 25, marginBottom: 10, padding: 10 }}>
              Vous n'avez encore aucun favori !
            </Text>
          </>
        )}
      </ScrollView>
    </SafeAreaView>
  );
};

export default LikeScreen;
