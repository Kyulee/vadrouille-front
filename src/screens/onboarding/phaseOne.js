import React from "react";
import { StyleSheet, Image, View, Button, Text } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "space-evenly",
    backgroundColor: "#fff",
  },
  image: {
    width: 350,
    height: 200,
    resizeMode: "stretch",
    alignSelf: "center",
  },
  groupButtons: {
    justifyContent: "flex-end",
  },
});

const PhaseOne = () => (
  <SafeAreaView style={styles.container}>
    <Image source={require("../../../assets/logo.png")} style={styles.image} />
    <View style={styles.groupButtons}>
      <Text>Bienvenue</Text>
      <Text>
        Dans la Vadrouille, vous pouvez dès maintenant réserver vos activités en
        quelques cliques.
      </Text>
      <Text>Je reserve :</Text>
      <Button title="Pour moi" />
      <Button title="Pour quelqu'un d'autre" />
    </View>
  </SafeAreaView>
);

export default PhaseOne;
