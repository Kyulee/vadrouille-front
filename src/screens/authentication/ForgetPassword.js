import React, { useState } from "react";
import { View, StyleSheet, Text, Button, Image } from "react-native";
import { Icon, Input } from "react-native-elements";
import Auth from "@aws-amplify/auth";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "flex-start",
    paddingTop: 100,
  },
  errorMessage: {
    color: "#FD6769",
  },
});

function ForgetPassword({ navigation }) {
  const [email, onChangeEmail] = useState("");
  const [editableInput, setEditableInput] = useState(true);
  const [confirmationStep, setConfirmationStep] = useState(false);
  const [code, setCode] = useState("");
  const [newPassword, setNewPassword] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const [showPassword, setShowPassword] = useState(true);

  const getConfirmationCode = async () => {
    if (email.length > 4) {
      Auth.forgotPassword(email)
        .then(() => {
          setEditableInput(true);
          setConfirmationStep(true);
          setErrorMessage("Une erreur s'est produite, veuillez réessayer.");
        })
        .catch((err) => {
          if (err.message) {
            setErrorMessage(err.message);
          }
        });
    } else {
      setErrorMessage("Merci de renseigner un email valide.");
    }
  };

  const postNewPassword = async () => {
    Auth.forgotPasswordSubmit(email, code, newPassword)
      .then(() => {
        setErrorMessage("Une erreur s'est produite, veuillez réessayer.");
        navigation.navigate("SignIn");
      })
      .catch((err) => {
        if (err.message) {
          setErrorMessage(err.message);
        }
      });
  };

  return (
    <KeyboardAvoidingView style={styles.container} behavior="padding">
      <Image
        source={require("../../../assets/logo.png")}
        style={{
          marginTop: 10,
          width: 90,
          height: 50,
          resizeMode: "stretch",
          alignSelf: "center",
        }}
      />
      <Text style={{ fontSize: 25, marginBottom: 10, padding: 10 }}>
        Restauration du mot de passe
      </Text>
      {!confirmationStep && (
        <>
          <Input
            value={email}
            placeholder="email@example.com"
            onChangeText={(text) => onChangeEmail(text)}
            editable={editableInput}
            autoCompleteType="email"
            autoCapitalize="none"
            autoFocus
            keyboardType="email-address"
          />
          <Button
            onPress={() => getConfirmationCode()}
            title="Restaurer votre de passe"
          />
        </>
      )}
      {confirmationStep && (
        <>
          <Text>Le code de confirmation viens d'être envoyé.</Text>
          <Text>Veuillez récuperer le code reçu et le copier ci-dessous.</Text>

          <Input
            value={code}
            placeholder="123456"
            onChangeText={(text) => setCode(text)}
          />
          <Text>Nouveau mot de passe</Text>
          <Input
            value={newPassword}
            placeholder="Mot de passe"
            errorMessage="Votre mot de passe doit contenir au moins 8 caractères."
            onChangeText={(text) => setNewPassword(text)}
            secureTextEntry={showPassword ? true : false}
            autoCompleteType="password"
            rightIcon={
              showPassword ? (
                <Icon
                  type="ionicon"
                  name="eye-outline"
                  onPress={() => setShowPassword(false)}
                />
              ) : (
                <Icon
                  type="ionicon"
                  name="eye-off-outline"
                  onPress={() => setShowPassword(true)}
                />
              )
            }
          />
          <Button
            onPress={() => postNewPassword()}
            title="Envoyer mon nouveau mot de passe"
          />
        </>
      )}
      <Button
        onPress={() => navigation.navigate("Welcome")}
        color="#F398BB"
        title="Annuler"
      />
      <Text style={styles.errorMessage}>{errorMessage}</Text>
    </KeyboardAvoidingView>
  );
}

export default ForgetPassword;
