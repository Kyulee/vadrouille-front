import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  Image,
  View,
  Button,
  KeyboardAvoidingView,
  ScrollView,
} from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import { SocialIcon, Icon, Input } from "react-native-elements";
import Auth from "@aws-amplify/auth";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "flex-start",
    paddingTop: 50,
  },
  button: {
    marginTop: 10,
    flexDirection: "row",
    justifyContent: "center",
  },
  image: {
    marginTop: 10,
    width: 350,
    height: 200,
    resizeMode: "stretch",
    alignSelf: "center",
  },
});

export default function SignUp({ navigation }) {
  const [name, onChangeName] = useState("");
  const [lastname, onChangeLastname] = useState("");
  const [email, onChangeEmail] = useState("");
  const [password, onChangePassword] = useState("");
  const [repeatPassword, onChangeRepeatPassword] = useState("");

  const [invalidMessage, setInvalidMessage] = useState(null);
  const [showPassword, setShowPassword] = useState(true);
  const [showConfirmPassword, setShowConfirmPassword] = useState(true);

  const signUp = async () => {
    const validPassword = password.length > 8 && password === repeatPassword;
    if (validPassword) {
      setInvalidMessage(null);
      Auth.signUp({
        username: email,
        password,
        attributes: {
          email, // optional
          name,
          // lastname
        },
        validationData: [], // optional
      })
        .then((data) => {
          console.log(data);
          console.log("navigation: ", navigation);
          navigation.navigate("Confirmation", { email });
        })
        .catch((err) => {
          if (err.message) {
            setInvalidMessage(err.message);
          }
          console.log(err);
        });
    } else {
      setInvalidMessage(
        "Le mot de passe doit contenir 8 ou plus de 8 caractères."
      );
    }
  };

  return (
    <KeyboardAvoidingView
      //      keyboardVerticalOffset={Header.HEIGHT + 20}
      style={{ flex: 1, marginTop: 70 }}
      behavior="padding"
    >
      <ScrollView>
        <Image
          source={require("../../../assets/logo.png")}
          style={styles.image}
        />
        {/* <View style={styles.button}>
        <SocialIcon type="facebook" />
        <SocialIcon type="google" />
        </View>
      <Text>OU</Text> */}
        <Input
          value={name}
          placeholder="Prénom"
          onChangeText={(text) => onChangeName(text)}
          autoFocus
        />
        <Input
          value={email}
          placeholder="email@example.com"
          onChangeText={(text) => onChangeEmail(text)}
          autoCapitalize="none"
          autoCompleteType="email"
          keyboardType="email-address"
        />
        <Input
          value={password}
          placeholder="Mot de passe"
          errorMessage="Votre mot de passe doit au moins contenir 8 caractères."
          onChangeText={(text) => onChangePassword(text)}
          secureTextEntry={showPassword ? true : false}
          autoCompleteType="password"
          rightIcon={
            showPassword ? (
              <Icon
                type="ionicon"
                name="eye-outline"
                onPress={() => setShowPassword(false)}
              />
            ) : (
              <Icon
                type="ionicon"
                name="eye-off-outline"
                onPress={() => setShowPassword(true)}
              />
            )
          }
        />
        <Input
          value={repeatPassword}
          errorMessage="Votre mot de passe doit correspondre au précedent."
          placeholder="Confirmation mot de passe"
          onChangeText={(text) => onChangeRepeatPassword(text)}
          secureTextEntry={showConfirmPassword ? true : false}
          autoCompleteType="password"
          shake
          rightIcon={
            showConfirmPassword ? (
              <Icon
                type="ionicon"
                name="eye-outline"
                onPress={() => setShowConfirmPassword(false)}
              />
            ) : (
              <Icon
                type="ionicon"
                name="eye-off-outline"
                onPress={() => setShowConfirmPassword(true)}
              />
            )
          }
        />

        <View style={styles.button}>
          <Button onPress={() => signUp()} color="#01B091" title="S'inscrire" />
          <Button
            onPress={() => navigation.navigate("Welcome")}
            color="#F398BB"
            title="Annuler"
          />
        </View>
        <Text>{invalidMessage}</Text>
      </ScrollView>
    </KeyboardAvoidingView>
  );
}
