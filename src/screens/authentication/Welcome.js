import React from "react";
import { StyleSheet, Image, View, Button } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "space-evenly",
    backgroundColor: "#fff",
  },
  image: {
    width: 350,
    height: 200,
    resizeMode: "stretch",
    alignSelf: "center",
  },
  groupButtons: {
    justifyContent: "flex-end",
  },
});

const Welcome = ({ navigation }) => (
  <SafeAreaView style={styles.container}>
    <Image source={require("../../../assets/logo.png")} style={styles.image} />
    <View style={styles.groupButtons}>
      <Button
        onPress={() => navigation.navigate("SignIn")}
        color="#01B091"
        title="Se connecter"
      />
      <Button
        onPress={() => navigation.navigate("SignUp")}
        color="#01B091"
        title="S'inscrire"
      />
    </View>
  </SafeAreaView>
);

export default Welcome;
