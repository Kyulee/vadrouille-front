/* eslint-disable no-console */
import React, { useState } from "react";
import PropTypes from "prop-types";
import {
  View,
  StyleSheet,
  Alert,
  Text,
  Image,
  Button,
  KeyboardAvoidingView,
  ScrollView,
} from "react-native";
import { Icon, SocialIcon, Input } from "react-native-elements";
import Auth from "@aws-amplify/auth";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "flex-start",
    paddingTop: 100,
  },
  button: {
    marginTop: 10,
    flexDirection: "row",
    justifyContent: "center",
  },
  image: {
    marginTop: 10,
    width: 350,
    height: 200,
    resizeMode: "stretch",
    alignSelf: "center",
  },
  errorMessage: {
    color: "#C70039",
    textAlign: "center",
  },
});

export default function SignIn({ navigation, signIn: signInCb }) {
  const [email, onChangeEmail] = useState("");
  const [password, onChangePassword] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const [showPassword, setShowPassword] = useState(true);

  const signIn = async () => {
    if (email.length > 4 && password.length > 2) {
      await Auth.signIn(email, password)
        .then((user) => {
          signInCb(user);
        })
        .catch((err) => {
          if (!err.message) {
            console.log("1 Error when signing in: ", err);
            Alert.alert("Error when signing in: ", err);
          } else {
            if (err.code === "UserNotConfirmedException") {
              console.log("User not confirmed");
              navigation.navigate("Confirmation", {
                email,
              });
            }
            if (err.message) {
              setErrorMessage(err.message);
            }
          }
        });
    } else {
      setErrorMessage(
        "Merci de renseigner une adresse email et un mot de passe valide."
      );
    }
  };

  return (
    <KeyboardAvoidingView
      // keyboardVerticalOffset={20}
      style={{ flex: 1, marginTop: 70 }}
      behavior="padding"
    >
      <ScrollView>
        <Image
          source={require("../../../assets/logo.png")}
          style={styles.image}
        />
        {/* <View style={styles.button}>
        <SocialIcon type="facebook" />
        <SocialIcon type="google" />
      </View>
      <Text>OU</Text> */}
        <Input
          value={email}
          placeholder="email@example.com"
          onChangeText={(text) => onChangeEmail(text)}
          autoCompleteType="email"
          autoCapitalize="none"
          autoFocus
          keyboardType="email-address"
        />
        <Input
          value={password}
          placeholder="Mot de passe"
          onChangeText={(text) => onChangePassword(text)}
          secureTextEntry={showPassword ? true : false}
          autoCompleteType="password"
          rightIcon={
            showPassword ? (
              <Icon
                type="ionicon"
                name="eye-outline"
                onPress={() => setShowPassword(false)}
              />
            ) : (
              <Icon
                type="ionicon"
                name="eye-off-outline"
                onPress={() => setShowPassword(true)}
              />
            )
          }
        />

        <Text style={styles.errorMessage}>{errorMessage}</Text>
        <View style={styles.button}>
          <Button onPress={() => signIn()} title="Se connecter" />
          <Button
            onPress={() => navigation.navigate("Welcome")}
            color="#F398BB"
            title="Annuler"
          />
        </View>
        <Button
          onPress={() => navigation.navigate("ForgetPassword")}
          color="#01B091"
          title="Mot de passe oublié ?"
        />
      </ScrollView>
    </KeyboardAvoidingView>
  );
}

SignIn.propTypes = {
  signIn: PropTypes.func.isRequired,
};
