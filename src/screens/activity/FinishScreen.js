import { Text, View, Button, Image } from "react-native";
import { useNavigation } from "@react-navigation/native";

import React, { useState } from "react";

const RecapScreen = ({ activity }) => {
  const { date, heure, activityDetails } = activity;
  const navigation = useNavigation();
  return (
    <View style={{ flex: 1, padding: 10 }}>
      <Image
        source={require("../../../assets/logo.png")}
        style={{ width: 190, height: 110, alignSelf: "center" }}
      />
      <Text style={{ fontSize: 20, textAlign: "center", marginVertical: 20 }}>
        Félicitation ! {"\n"}
        {"\n"}Votre paiement de{" "}
        <Text style={{ color: "#01B091" }}>{activityDetails.price} €</Text> a
        bien été pris en compte.
      </Text>

      <Text style={{ fontSize: 20, textAlign: "center", marginBottom: 10 }}>
        L'activité{" "}
        <Text style={{ color: "#01B091" }}>
          "{activityDetails.name}" aura lieu le {date} de {heure}.
        </Text>
      </Text>
      <Text style={{ fontSize: 12, textAlign: "center", marginBottom: 50 }}>
        Vous allez recevoir un mail avec le récapitulatif de votre commande.
        Nous avons hate de vous faire participer a nos activités proposées !
      </Text>
      <Button
        title="Retourner à l'accueil"
        onPress={() => {
          navigation.navigate("Home");
        }}
      />
    </View>
  );
};

export default RecapScreen;
