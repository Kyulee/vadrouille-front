import {
  Text,
  View,
  Button,
  ImageBackground,
  TextInput,
  TouchableOpacity,
  SafeAreaView,
  Image,
} from "react-native";
import { Card } from "react-native-elements";
import React, { useState } from "react";
import { ScrollView } from "react-native-gesture-handler";
import { ProgressSteps, ProgressStep } from "react-native-progress-steps";
import RecapScreen from "./RecapScreen";
import PaymentScreen from "./PaymentScreen";
import FinishScreen from "./FinishScreen";
import { Avatar } from "react-native-elements";

const BookingScreen = ({ route, navigation }) => {
  const { activityDetails } = route.params;
  const [activity, setActivity] = useState();
  const [activeStep, setActiveStep] = useState(0);

  const disponibility = [
    {
      id: 1,
      date: "Jeudi 22 Juin",
      heure: "10h - 11h",
      stock: 2,
    },
    {
      id: 2,
      date: "Jeudi 22 Juin",
      heure: "11h - 12h",
      stock: 10,
    },
    {
      id: 3,
      date: "Jeudi 22 Juin",
      heure: "12h - 13h",
      stock: 0,
    },
    {
      id: 4,
      date: "Vendredi 23 Juin",
      heure: "14h - 15h",
      stock: 5,
    },
  ];
  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "white" }}>
      <Image
        source={require("../../../assets/logo.png")}
        style={{ width: 95, height: 55, alignSelf: "center" }}
      />
      <Text style={{ textAlign: "center", fontSize: 20 }}>
        Réservation de votre activité
      </Text>
      <ProgressSteps activeStep={activeStep}>
        <ProgressStep label="Créneaux" removeBtnRow>
          <View style={{ alignItems: "center" }}>
            <ScrollView style={{ flex: 2 }}>
              {disponibility.map((dispo) => {
                return (
                  <View key={dispo.id}>
                    <TouchableOpacity
                      disabled={!dispo.stock}
                      onPress={() => {
                        setActivity({
                          id: dispo.id,
                          date: dispo.date,
                          heure: dispo.heure,
                          activityDetails: activityDetails,
                        });
                        setActiveStep(1);
                      }}
                    >
                      <Card color="danger" outline>
                        <Card.Title
                          style={{
                            fontSize: 20,
                          }}
                        >
                          {dispo.date}
                        </Card.Title>
                        <Card.FeaturedSubtitle
                          style={{
                            color: "black",
                            textAlign: "center",
                            fontSize: 20,
                          }}
                        >
                          {dispo.heure}
                        </Card.FeaturedSubtitle>
                        <Text
                          style={{
                            color: dispo.stock === 0 ? "#F398BB" : "#01B091",
                            textAlign: "center",
                            fontSize: 20,
                          }}
                        >
                          {dispo.stock === 0
                            ? "Il n'y a plus de place disponible"
                            : `${dispo.stock} places disponibles`}
                        </Text>
                      </Card>
                    </TouchableOpacity>
                  </View>
                );
              })}
            </ScrollView>
          </View>
        </ProgressStep>
        <ProgressStep
          label="Récapitulatif"
          nextBtnText="Payer"
          previousBtnText="Revenir aux créneaux"
          onPrevious={() => {
            setActivity({});
            setActiveStep(0);
          }}
        >
          <View>
            <RecapScreen activity={activity} />
          </View>
        </ProgressStep>
        <ProgressStep
          label="Paiement"
          previousBtnText="Revenir aux récapitulatif"
        >
          <View style={{ alignItems: "center" }}>
            <PaymentScreen activity={activity} />
          </View>
        </ProgressStep>
        <ProgressStep label="C'est parti !" removeBtnRow>
          <View style={{ alignItems: "center" }}>
            <FinishScreen activity={activity} />
          </View>
        </ProgressStep>
      </ProgressSteps>
    </SafeAreaView>
  );
};

export default BookingScreen;
