import { Text, View, Button } from "react-native";
import React, { useState } from "react";
import { CardField, useStripe } from "@stripe/stripe-react-native";

const PaymentScreen = ({ route, navigation }) => {
  const [card, setCard] = useState(null);

  return (
    <View>
      <CardField
        postalCodeEnabled={true}
        placeholder={{
          number: "4242 4242 4242 4242",
        }}
        cardStyle={{
          backgroundColor: "#FFFFFF",
          textColor: "#000000",
        }}
        style={{
          width: 350,
          height: 50,
          marginVertical: 30,
        }}
        onCardChange={(cardDetails) => {
          setCard(cardDetails);
        }}
        onFocus={(focusedField) => {
          console.log("focusField", focusedField);
        }}
      />
      <Button title="Validation de mon Paiement" onPress={() => {}} />
    </View>
  );
};

export default PaymentScreen;
