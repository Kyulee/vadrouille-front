import { View, Text, ImageBackground } from "react-native";
import { Icon } from "react-native-elements";
import React, { useState } from "react";
import CardActivite from "../../components/CardActivite";
import { ScrollView } from "react-native-gesture-handler";
import { env } from "../../../environment";

const CategoryScreen = ({ route, navigation }) => {
  const { category } = route.params;
  const [count, setCount] = useState(0);

  return (
    <View>
      <ImageBackground
        source={{ uri: category.topPhoto }}
        style={{
          width: 400,
          height: 150,
          alignSelf: "center",
          overflow: "hidden",
          marginBottom: 10,
        }}
      >
        <View
          style={{
            backgroundColor: "white",
            width: 50,
            height: 40,
            borderRadius: 15,
            marginLeft: 30,
            marginTop: 40,
            justifyContent: "center",
            borderColor: "#fff",
            borderWidth: 1,
          }}
        >
          <Icon
            name={"chevron-left"}
            size={30}
            onPress={() => navigation.goBack()}
          />
        </View>
      </ImageBackground>
      <View style={{ alignItems: "center" }}>
        <Text style={{ fontSize: 30, color: "#01B091" }}>{category.title}</Text>
        <Text style={{ fontSize: 15, color: "grey", fontStyle: "italic" }}>
          {category.desc}
        </Text>
      </View>
      <Text style={{ fontSize: 20, textAlign: "center", marginBottom: 10 }}>
        {count} activités pres de chez vous !
      </Text>
      <ScrollView style={{ marginBottom: 220 }}>
        <CardActivite category={category.id} setCount={setCount} />
      </ScrollView>
    </View>
  );
};

export default CategoryScreen;
