import { Text, View, Button, ImageBackground, Image } from "react-native";
import { Icon, Divider, Rating } from "react-native-elements";
import React, { useState, useEffect } from "react";
import { ScrollView } from "react-native-gesture-handler";
import storage from "../../services/storage";

const ActivityScreen = ({ route, navigation }) => {
  const { activityDetails } = route.params;

  const [like, setLike] = useState(false);

  useEffect(() => {
    setLike(activityDetails.isLiked);
  }, [activityDetails]);

  return (
    <View style={{ flex: 1 }}>
      <ImageBackground
        source={{ uri: activityDetails.uri }}
        style={{
          width: 400,
          height: 200,
          alignSelf: "center",
          overflow: "hidden",
        }}
      >
        <View
          style={{
            backgroundColor: "white",
            width: 50,
            height: 40,
            borderRadius: 15,
            marginLeft: 30,
            marginTop: 40,
            justifyContent: "center",
            borderColor: "#fff",
            borderWidth: 1,
          }}
        >
          <Icon
            name={"chevron-left"}
            size={30}
            onPress={() => navigation.goBack()}
          />
        </View>
      </ImageBackground>
      <ScrollView style={{ marginLeft: 10 }}>
        <View
          style={{
            flexDirection: "row",
            alignItems: "space-between",
            margin: 10,
          }}
        >
          <Text style={{ fontSize: 25, flex: 1, fontWeight: "bold" }}>
            {activityDetails.name}
          </Text>
          {like ? (
            <Icon
              name="heart"
              type="font-awesome"
              color="#f398bb"
              size={40}
              style={{
                alignSelf: "flex-end",
                marginRight: 10,
                marginTop: 10,
              }}
              onPress={() => {
                setLike(false);
                storage.remove({
                  key: "like",
                  id: activityDetails.id,
                });
              }}
            />
          ) : (
            <Icon
              name="heart"
              type="font-awesome"
              color="#01B091"
              size={40}
              style={{
                alignSelf: "flex-end",
                marginRight: 10,
                marginTop: 10,
              }}
              onPress={() => {
                setLike(true);
                storage.save({
                  key: "like",
                  id: activityDetails.id,
                  data: { ...activityDetails, isLiked: true },
                });
              }}
            />
          )}
        </View>
        <View
          style={{
            flexDirection: "row",
            marginBottom: 20,
            marginTop: 20,
            marginHorizontal: 30,
            justifyContent: "space-evenly",
          }}
        >
          <Icon name="map-marker" type="font-awesome" color="#01B091" />
          <Text>{activityDetails.distance} km</Text>
          <Icon name="star" type="font-awesome" color="#f9C62D" />
          <Text>{activityDetails.rate}</Text>
          <Icon name="clock-o" type="font-awesome" color="#f23243" />
          <Text>{activityDetails.duration} min</Text>
        </View>
        <Text style={{ flex: 1, fontStyle: "italic" }}>
          {activityDetails.description}
        </Text>
        <Divider style={{ marginVertical: 20 }} />
        <Text style={{ fontSize: 20 }}>Adaptée aux</Text>
        <View
          style={{
            padding: 10,
            flexDirection: "row",
            justifyContent: "space-between",
            alignContent: "center",
            textAlign: "center",
          }}
        >
          <View
            style={{
              padding: 10,
              alignItems: "center",
            }}
          >
            <Image
              source={{
                uri: "/Users/kyulee/Desktop/vadrouille/vadrouille-front/assets/alzheimericon.png",
              }}
              style={{ width: 44, height: 42 }}
            />
            <Text>Malentendants</Text>
          </View>
          <View
            style={{
              padding: 25,
              alignItems: "center",
            }}
          >
            <Image
              source={{
                uri: "/Users/kyulee/Desktop/vadrouille/vadrouille-front/assets/mueticon.png",
              }}
              style={{ width: 31, height: 27 }}
            />
            <Text>Muets</Text>
          </View>
          <View
            style={{
              padding: 12,
              alignItems: "center",
            }}
          >
            <Image
              source={{
                uri: "/Users/kyulee/Desktop/vadrouille/vadrouille-front/assets/brasprotheseicon.png",
              }}
              style={{ width: 27, height: 39 }}
            />
            <Text>Handicapés moteurs</Text>
          </View>
        </View>
        <Divider style={{ marginVertical: 10 }} />
        <Text style={{ fontSize: 20 }}>Lieu de l'activité </Text>
        <View
          style={{
            flexDirection: "row",
            marginHorizontal: 30,
            padding: 10,
          }}
        >
          <Icon
            name="map-marker"
            type="font-awesome"
            color="#01B091"
            style={{ marginRight: 10, alignSelf: "center" }}
          />
          <Text>{activityDetails.address}</Text>
        </View>
        <Text>{activityDetails.extraDescription}</Text>
        <Divider style={{ marginVertical: 20 }} />
        <Text style={{ fontSize: 20, marginBottom: 10 }}>Avis client</Text>
        <Text style={{ fontWeight: "bold" }}>Maria B.</Text>
        <Rating
          type="custom"
          ratingBackgroundColor="white"
          imageSize={20}
          readonly
          ratingColor="#F9C62D"
        />
        <Text>Il y a 3 jours</Text>
      </ScrollView>
      <View
        style={{
          flexDirection: "row",
          borderColor: "lightgrey",
          borderWidth: 1,
        }}
      >
        <Text style={{ width: "50%", fontSize: 20, textAlign: "center" }}>
          {activityDetails.price} € TTC / pers
        </Text>
        <Button
          style={{ width: "50%", alignContent: "center" }}
          onPress={() => {
            navigation.navigate("Booking", {
              activityDetails: activityDetails,
            });
          }}
          title="Je réserve"
        />
      </View>
    </View>
  );
};

export default ActivityScreen;
