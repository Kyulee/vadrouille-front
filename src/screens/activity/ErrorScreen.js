import { Text, View, Button, TextInput } from "react-native";
import { Avatar } from "react-native-elements";
import React, { useState } from "react";

const RecapScreen = () => {
  return (
    <View style={{ flex: 1 }}>
      <Text>
        Une erreur s'est produite. Merci de réessayer votre réservation.
      </Text>
    </View>
  );
};

export default RecapScreen;
