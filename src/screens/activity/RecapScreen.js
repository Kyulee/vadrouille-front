import { Text, View, Button, TextInput } from "react-native";
import { Avatar, Divider, Input, CheckBox, Icon } from "react-native-elements";
import React, { useState } from "react";

const RecapScreen = ({ activity, navigation }) => {
  const [text, onChangeText] = React.useState("");
  const [isSelected, setIsSelected] = useState(false);

  const { date, heure, activityDetails } = activity;
  return (
    <View style={{ flex: 1 }}>
      <Text style={{ fontSize: 20, textAlign: "center" }}>
        Récapitulatif de la réservation
      </Text>
      <Divider
        width={1}
        inset
        insetType="middle"
        style={{ marginVertical: 20 }}
      />
      <View style={{ padding: 10 }}>
        <Text
          style={{
            fontSize: 20,
            marginBottom: 10,
            textDecorationLine: "underline",
          }}
        >
          Activité choisi :
        </Text>
        <Text
          style={{
            fontSize: 15,
          }}
        >
          {activityDetails.name}
        </Text>
        <Text
          style={{
            fontSize: 20,
            marginVertical: 10,
            textDecorationLine: "underline",
          }}
        >
          Session :
        </Text>
        <Text
          style={{
            fontSize: 15,
          }}
        >
          Le {date} de {heure}
        </Text>
        <Text
          style={{
            fontSize: 20,
            marginVertical: 10,
            textDecorationLine: "underline",
          }}
        >
          Inscrit :
        </Text>
        <View style={{ padding: 10 }}>
          <Avatar
            size={40}
            rounded
            icon={{ name: "user", type: "font-awesome" }}
            containerStyle={{ backgroundColor: "#3d4db7" }}
          />
          <Text>Vous</Text>
        </View>
        {/* <Avatar
          size={40}
          rounded
          title="+"
          containerStyle={{ backgroundColor: "#F9C62D" }}
        /> */}
      </View>
      <Divider
        width={1}
        inset
        insetType="middle"
        style={{ marginVertical: 20 }}
      />
      {/* <Text style={{ fontSize: 20 }}>Code promotionnel</Text>
      <Input
        value={text}
        onChangeText={onChangeText}
        rightIcon={
          <Icon
            name="add-circle-outline"
            type="ionicon"
            size={24}
            color="#01B091"
          />
        }
      /> */}
      <CheckBox
        checked={isSelected}
        onPress={() => setIsSelected(!isSelected)}
        center
        title="J'accepte les conditions de vente et de politique de confidentialité de La Vadrouille"
      />
      <Text style={{ fontSize: 20, textAlign: "center", marginVertical: 20 }}>
        Total : {activityDetails.price} € TTC
      </Text>
    </View>
  );
};

export default RecapScreen;
