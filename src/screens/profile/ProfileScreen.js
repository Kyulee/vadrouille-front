import React, { useState } from "react";
import { View, Text, ImageBackground, Button } from "react-native";
import { Avatar, Icon, ListItem } from "react-native-elements";
import { ScrollView } from "react-native-gesture-handler";
import Auth from "@aws-amplify/auth";

const ProfileScreen = ({ navigation, signOut }) => {
  const [username, setUsername] = useState("");
  const [profilePicture, setProfilePicture] = useState("");
  const [nextActivite, setNextActivite] = useState({});
  async function getUsername() {
    try {
      let info = await Auth.currentSession();
      setUsername(info.idToken.payload["name"]);
      setProfilePicture(info.idToken.payload.picture);
    } catch (err) {
      console.log(err);
    }
  }
  getUsername();

  const list = [
    {
      id: 1,
      title: "Informations personnelles",
      icon: "person",
      linkTo: "Mon compte",
    },
    {
      id: 2,
      title: "Mes réservations",
      icon: "bookmark",
      linkTo: "BookingActivityScreen",
    },
    // {
    //   id: 3,
    //   title: "Promotions",
    //   icon: "bookmark",
    //   linkTo: "Promotion",
    // },
    // {
    //   id: 4,
    //   title: "Accessibilité",
    //   icon: "fitness",
    //   linkTo: "Acessibility",
    // },
    {
      id: 5,
      title: "F.A.Q.",
      icon: "help",
      linkTo: "Faq",
    },
    {
      id: 6,
      title: "A propos",
      icon: "information",
      linkTo: "AboutUs",
    },
  ];
  return (
    <View>
      <ImageBackground
        source={require("../../../assets/profileImage.png")}
        style={{
          width: 400,
          height: 150,
          alignSelf: "center",
          borderBottomLeftRadius: 30,
          borderBottomRightRadius: 30,
        }}
      >
        <View style={{ alignSelf: "center", marginTop: 100 }}>
          <Avatar
            size={100}
            rounded
            source={{
              uri: profilePicture,
            }}
          />
        </View>
      </ImageBackground>
      <Text
        style={{
          fontSize: 25,
          marginTop: 50,
          marginBottom: 10,
          padding: 10,
          textAlign: "center",
        }}
      >
        Bonjour {username} !
      </Text>
      <View
        style={{
          paddingTop: 10,
          borderColor: "grey",
          borderWidth: 1,
          width: 250,
          borderRadius: 5,
          alignItems: "center",
          alignSelf: "center",
        }}
      >
        <Icon color="#f398bb" name="rowing" size={50} />
        <Text
          style={{
            fontSize: 20,
            padding: 10,
            textAlign: "center",
          }}
        >
          Ma prochaine activité
        </Text>
        {!nextActivite ? (
          <>
            <Text
              style={{
                fontSize: 15,
                color: "grey",
              }}
            >
              Prochaine actvité
            </Text>
            <Text
              style={{
                fontSize: 15,
                color: "grey",
              }}
            >
              15/09/2022
            </Text>
            <Button title="Voir plus" />
          </>
        ) : (
          <Text
            style={{
              fontSize: 15,
              padding: 10,
              color: "grey",
            }}
          >
            Vous n'avez aucune activité :(
          </Text>
        )}
      </View>
      <ScrollView>
        <View style={{ marginTop: 10 }}>
          {list.map((l) => (
            <ListItem
              key={l.id}
              onPress={() => navigation.navigate(`${l.linkTo}`)}
              bottomDivider
            >
              <Icon color="#01B091" name={l.icon} type="ionicon" />
              <ListItem.Content>
                <ListItem.Title>{l.title}</ListItem.Title>
              </ListItem.Content>
              <ListItem.Chevron />
            </ListItem>
          ))}
        </View>
        <Button onPress={() => signOut()} title="Déconnexion" />
      </ScrollView>
    </View>
  );
};

export default ProfileScreen;
