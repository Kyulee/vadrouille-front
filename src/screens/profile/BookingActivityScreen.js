import {
  SafeAreaView,
  View,
  Image,
  Text,
  ScrollView,
  ImageBackground,
} from "react-native";
import React, { useState } from "react";
import { Icon } from "react-native-elements";

const BookingActivityScreen = ({ navigation }) => {
  const [booking, setBooking] = useState([]);
  return (
    <SafeAreaView>
      <View>
        <ImageBackground
          source={require("../../../assets/profileImage.png")}
          style={{
            width: 400,
            height: 150,
            alignSelf: "center",
            borderBottomLeftRadius: 30,
            borderBottomRightRadius: 30,
          }}
        >
          <View
            style={{
              backgroundColor: "white",
              width: 50,
              height: 40,
              borderRadius: 15,
              marginLeft: 30,
              marginTop: 10,
              justifyContent: "center",
              borderColor: "#fff",
              borderWidth: 1,
            }}
          >
            <Icon
              name={"chevron-left"}
              size={30}
              onPress={() => navigation.goBack()}
            />
          </View>
          <View
            style={{
              backgroundColor: "white",
              alignSelf: "center",
              marginTop: 80,
              borderRadius: 15,
              justifyContent: "center",
              borderColor: "#fff",
              borderWidth: 1,
              padding: 10,
            }}
          >
            <Text
              style={{
                fontSize: 25,
                color: "#01B091",
              }}
            >
              Mes réservations
            </Text>
          </View>
        </ImageBackground>
      </View>

      <ScrollView style={{ marginTop: 60 }}>
        {booking.map((activity) => (
          <Activite
            key={activity.id}
            activite={activity}
            isLiked={activity.isLiked}
          />
        ))}
        {!booking.length && (
          <>
            <Image
              source={{
                uri: "https://cdn-icons-png.flaticon.com/512/594/594848.png?w=1380",
              }}
              style={{
                width: 50,
                height: 50,
                alignSelf: "center",
                resizeMode: "stretch",
              }}
            />
            <Text
              style={{
                fontSize: 25,
                marginBottom: 10,
                padding: 10,
                textAlign: "center",
              }}
            >
              Vous n'avez encore aucune réservation !
            </Text>
          </>
        )}
      </ScrollView>
    </SafeAreaView>
  );
};

export default BookingActivityScreen;
