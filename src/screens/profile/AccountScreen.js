import React, { useState } from "react";
import {
  View,
  Text,
  ImageBackground,
  TextInput,
  Button,
  StyleSheet,
} from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import { Icon } from "react-native-elements";

const styles = StyleSheet.create({
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: "grey",
    padding: 10,
  },
  profilePicture: {
    width: 400,
    height: 200,
    alignSelf: "center",
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
    overflow: "hidden",
  },
  backButton: {
    backgroundColor: "white",
    width: 50,
    height: 40,
    borderRadius: 15,
    marginLeft: 30,
    marginTop: 10,
    justifyContent: "center",
    borderColor: "#fff",
    borderWidth: 1,
  },
  iconButton: {
    fontSize: 100,
  },
  modifyPhotoButton: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "flex-end",
  },
  labelText: {
    fontSize: 20,
    marginLeft: 10,
  },
});

const AccountScreen = ({ navigation }) => {
  const [email, onChangeEmail] = useState("mauricette.gigi@gmail.com");
  const [name, onChangeName] = useState("Mauricette");
  const [lastname, onChangeLastname] = useState("Gigi");
  const [dateOfBirth, onChangeDateOfBirth] = useState(new Date());

  return (
    <SafeAreaView>
      <View>
        <ImageBackground
          source={{
            uri: "https://randomuser.me/api/portraits/med/women/0.jpg",
          }}
          style={styles.profilePicture}
        >
          <View style={styles.backButton}>
            <Icon
              name={"chevron-left"}
              size={30}
              onPress={() => navigation.goBack()}
            />
          </View>
          <View style={styles.modifyPhotoButton}>
            <Button color="#01B091" title="Modifier la photo" />
          </View>
        </ImageBackground>
        <View style={{ flexDirection: "row", flexGrow: 1, marginTop: 10 }}>
          <View style={{ flex: 1 }}>
            <Text style={styles.labelText}>Prénom</Text>
            <TextInput
              style={styles.input}
              value={lastname}
              onChangeText={(text) => onChangeLastname(text)}
              autoFocus
            />
          </View>
          <View style={{ flex: 2 }}>
            <Text style={styles.labelText}>Nom</Text>
            <TextInput
              style={styles.input}
              value={name}
              onChangeText={(text) => onChangeName(text)}
              autoFocus
            />
          </View>
        </View>
        <Text style={styles.labelText}>Adresse email</Text>
        <TextInput
          style={styles.input}
          value={email}
          placeholder="email@example.com"
          onChangeText={(text) => onChangeEmail(text)}
          autoCompleteType="email"
          autoCapitalize="none"
          autoFocus
          keyboardType="email-address"
        />
        <Text style={styles.labelText}>Date de naissance</Text>
        <TextInput style={styles.input} placeholder="21/05/1956" autoFocus />
        <Text style={styles.labelText}>Numero de téléphone</Text>
        <TextInput style={styles.input} placeholder="0612345678" autoFocus />
        <Button color="#01B091" title="Enregistrer" />
        <Button
          onPress={() => navigation.goBack()}
          color="#F398BB"
          title="Annuler"
        />
      </View>
    </SafeAreaView>
  );
};

export default AccountScreen;
