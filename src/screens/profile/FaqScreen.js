import {
  SafeAreaView,
  View,
  Image,
  Text,
  ScrollView,
  ImageBackground,
} from "react-native";
import React from "react";
import { Icon } from "react-native-elements";
import { List } from "react-native-paper";

const FaqScreen = ({ navigation }) => {
  const [expanded, setExpanded] = React.useState(false);

  const handlePress = () => setExpanded(!expanded);

  return (
    <SafeAreaView>
      <View>
        <ImageBackground
          source={require("../../../assets/profileImage.png")}
          style={{
            width: 400,
            height: 150,
            alignSelf: "center",
            borderBottomLeftRadius: 30,
            borderBottomRightRadius: 30,
          }}
        >
          <View
            style={{
              backgroundColor: "white",
              width: 50,
              height: 40,
              borderRadius: 15,
              marginLeft: 30,
              marginTop: 10,
              justifyContent: "center",
              borderColor: "#fff",
              borderWidth: 1,
            }}
          >
            <Icon
              name={"chevron-left"}
              size={30}
              onPress={() => navigation.goBack()}
            />
          </View>
          <View
            style={{
              backgroundColor: "white",
              alignSelf: "center",
              marginTop: 80,
              borderRadius: 15,
              justifyContent: "center",
              borderColor: "#fff",
              borderWidth: 1,
              padding: 10,
            }}
          >
            <Text
              style={{
                fontSize: 25,
                color: "#01B091",
              }}
            >
              Foire aux questions
            </Text>
          </View>
        </ImageBackground>
      </View>

      <ScrollView style={{ marginTop: 60 }}>
        <List.Section>
          <List.Accordion title="Comment réserver une activité ?">
            <List.Item description="Pour réserver une activité, il suffit de vous rendre sur une activité et de suivre le chemin de réservation." />
          </List.Accordion>

          <List.Accordion
            title="Quels sont les moyens de paiement acceptés ?"
            titleNumberOfLines={2}
            expanded={expanded}
            onPress={handlePress}
          >
            <List.Item description="Les moyens de paiement accepté sur l'application La Vadrouille sont : les cartes bleues Visa, Mastercard." />
          </List.Accordion>
        </List.Section>
      </ScrollView>
    </SafeAreaView>
  );
};

export default FaqScreen;
