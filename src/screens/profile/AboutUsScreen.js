import React, { useState } from "react";
import { View, Text, ImageBackground, SafeAreaView, Image } from "react-native";
import { Icon } from "react-native-elements";
import { ScrollView } from "react-native-gesture-handler";

const AboutUsScreen = ({ navigation }) => {
  return (
    <SafeAreaView>
      <View>
        <ImageBackground
          source={require("../../../assets/profileImage.png")}
          style={{
            width: 400,
            height: 150,
            alignSelf: "center",
            borderBottomLeftRadius: 30,
            borderBottomRightRadius: 30,
          }}
        >
          <View
            style={{
              backgroundColor: "white",
              width: 50,
              height: 40,
              borderRadius: 15,
              marginLeft: 30,
              marginTop: 10,
              justifyContent: "center",
              borderColor: "#fff",
              borderWidth: 1,
            }}
          >
            <Icon
              name={"chevron-left"}
              size={30}
              onPress={() => navigation.goBack()}
            />
          </View>
          <View
            style={{
              backgroundColor: "white",
              alignSelf: "center",
              marginTop: 80,
              borderRadius: 15,
              justifyContent: "center",
              borderColor: "#fff",
              borderWidth: 1,
              padding: 10,
            }}
          >
            <Text
              style={{
                fontSize: 25,
                color: "#01B091",
              }}
            >
              A propos de nous
            </Text>
          </View>
        </ImageBackground>
      </View>

      <ScrollView style={{ marginTop: 50, padding: 10, marginBottom: 100 }}>
        <View>
          <Text
            style={{
              fontWeight: "700",
              marginBottom: 10,
              fontSize: 20,
              color: "#01B091",
            }}
          >
            C'est quoi La Vadrouille ?
          </Text>
          <Text style={{ fontStyle: "italic" }}>
            La Vadrouille a vu le jour après de nombreuses séances de
            brainstorming. Après discussion, nous nous sommes accordées sur le
            secteur sur lequel nous souhaitions travailler : la silver economy.
            En effet, les séniors, retraités, personnes âgées ont bien évolué et
            leurs besoins aussi. Aujourd’hui, ils utilisent quotidiennement
            internet. Après analyse du secteur, nous avons tenté d’identifier
            leurs principaux problèmes et comment les solutionner grâce au
            digital. La problématique est beaucoup revenue sur la table, elle
            touche davantage les retraités en France. Le projet de La Vadrouille
            est donc né, après divers pivots.
          </Text>
          <Text
            style={{
              fontWeight: "700",
              marginVertical: 10,
              fontSize: 20,
              color: "#01B091",
            }}
          >
            Pourquoi la Vadrouille ?
          </Text>
          <Text style={{ fontStyle: "italic" }}>
            Afin de coller et définir un maximum notre cible, il fallait trouver
            un nom dynamique qui inspire l’aventure. De plus, un nom français
            d’application répondait davantage aux préférences de notre cible.
          </Text>
          <Text
            style={{
              fontWeight: "700",
              marginVertical: 10,
              fontSize: 20,
              color: "#01B091",
            }}
          >
            Qu’en est-il du légal ?
          </Text>
          <Text style={{ fontStyle: "italic" }}>
            Afin de Nous avons pour projet de lancer la Vadrouille en SAS. En
            effet, ce statut permet de diviser les parts de sociétés comme bon
            nous semble, de cette manière chacun des membres du groupe peut
            avoir une part égale.
          </Text>
          <Text
            style={{
              fontWeight: "700",
              marginVertical: 10,
              fontSize: 20,
              color: "#01B091",
            }}
          >
            L'équipe
          </Text>
          <Text style={{ fontStyle: "italic" }}>
            Afin de mener à bien ce projet, nous avons constitué une équipe
            composée de deux designeuses, une développeuse web et trois
            marketeuses.
          </Text>
          <Image
            source={require("../../../assets/equipe.png")}
            style={{
              marginTop: 10,
              marginBottom: 70,
              width: 320,
              height: 150,
              alignSelf: "center",
            }}
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default AboutUsScreen;
