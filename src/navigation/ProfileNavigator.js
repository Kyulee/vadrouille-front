import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import AccountScreen from "../screens/profile/AccountScreen";
import PaymentMethodScreen from "../screens/profile/BookingActivityScreen";
import AboutUsScreen from "../screens/profile/AboutUsScreen";
import FaqScreen from "../screens/profile/FaqScreen";
import PromotionScreen from "../screens/profile/PromotionScreen";
import AcessibilityScreen from "../screens/profile/AcessibilityScreen";
import ProfileScreen from "../screens/profile/ProfileScreen";
import BookingActivityScreen from "../screens/profile/BookingActivityScreen";

const ProfileStack = createStackNavigator();

const ProfileNavigator = ({ signOut }) => (
  <ProfileStack.Navigator initialRouteName="Compte">
    <ProfileStack.Screen name="ProfileScreen" options={{ headerShown: false }}>
      {({ navigation }) => (
        <ProfileScreen signOut={signOut} navigation={navigation} />
      )}
    </ProfileStack.Screen>

    <ProfileStack.Screen
      name="Mon compte"
      component={AccountScreen}
      options={({ navigation }) => ({
        headerShown: false,
      })}
    />
    <ProfileStack.Screen
      options={({ navigation }) => ({
        headerShown: false,
      })}
      name="BookingActivityScreen"
      component={BookingActivityScreen}
    />
    <ProfileStack.Screen name="Promotion" component={PromotionScreen} />
    <ProfileStack.Screen name="Acessibility" component={AcessibilityScreen} />
    <ProfileStack.Screen
      name="Faq"
      options={({ navigation }) => ({
        headerShown: false,
      })}
      component={FaqScreen}
    />
    <ProfileStack.Screen
      options={({ navigation }) => ({
        headerShown: false,
      })}
      name="AboutUs"
      component={AboutUsScreen}
    />
  </ProfileStack.Navigator>
);

export default ProfileNavigator;
