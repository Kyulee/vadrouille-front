import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import CategoryScreen from "../screens/activity/CategoryScreen";
import ActivityScreen from "../screens/activity/ActivityScreen";
import HomeScreen from "../screens/tab/HomeScreen";
import BookingScreen from "../screens/activity/BookingScreen";
import RecapScreen from "../screens/activity/RecapScreen";
import PaymentScreen from "../screens/activity/PaymentScreen";
import { StripeProvider } from "@stripe/stripe-react-native";

const ActivityStack = createStackNavigator();

const ActivityNavigator = () => (
  <StripeProvider publishableKey="pk_test_oKhSR5nslBRnBZpjO6KuzZeX">
    <ActivityStack.Navigator initialRouteName="Home">
      <ActivityStack.Screen
        name="Home"
        options={{ headerShown: false }}
        component={HomeScreen}
      />
      <ActivityStack.Screen
        name="Category"
        component={CategoryScreen}
        options={{ headerShown: false }}
      ></ActivityStack.Screen>
      <ActivityStack.Screen
        name="Activity"
        component={ActivityScreen}
        options={{ headerShown: false }}
      />
      <ActivityStack.Screen
        name="Booking"
        component={BookingScreen}
        options={{ headerShown: false }}
      />
      <ActivityStack.Screen name="Recap" component={RecapScreen} />
      <ActivityStack.Screen name="Payment" component={PaymentScreen} />
    </ActivityStack.Navigator>
  </StripeProvider>
);

export default ActivityNavigator;
