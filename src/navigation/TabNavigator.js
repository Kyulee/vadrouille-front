import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { Ionicons } from "@expo/vector-icons";
import SearchScreen from "../screens/tab/SearchScreen";
import NotificationScreen from "../screens/tab/NotificationScreen";
import LikeScreen from "../screens/tab/LikeScreen";
import ProfileNavigator from "./ProfileNavigator";
import ActivityNavigator from "./ActivityNavigator";

const Tab = createBottomTabNavigator();

export default function TabNavigator({ signOut }) {
  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        headerShown: false,
        tabBarIcon: ({ focused, color, size, style }) => {
          let iconName;

          if (route.name === "Accueil") {
            iconName = focused ? "home" : "home-outline";
          } else if (route.name === "Favoris") {
            iconName = focused ? "heart" : "heart-outline";
          } else if (route.name === "Recherche") {
            iconName = focused ? "search" : "search-outline";
            style = {};
          } else if (route.name === "Notification") {
            iconName = focused ? "notifications" : "notifications-outline";
          } else if (route.name === "Compte") {
            iconName = focused ? "person" : "person-outline";
          }

          return <Ionicons name={iconName} size={size} color={color} />;
        },
        tabBarActiveTintColor: "#f398bb",
        tabBarInactiveTintColor: "#01B091",
      })}
    >
      <Tab.Screen name="Accueil" children={() => <ActivityNavigator />} />
      <Tab.Screen name="Favoris" component={LikeScreen} />
      <Tab.Screen name="Recherche" component={SearchScreen} />
      <Tab.Screen
        name="Notification"
        component={NotificationScreen}
      // options={{
      //   tabBarBadge: mockDataNotif.length(),
      // }}
      />
      <Tab.Screen
        name="Compte"
        children={() => <ProfileNavigator signOut={signOut} />}
      />
    </Tab.Navigator>
  );
}
