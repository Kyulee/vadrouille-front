import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { SafeAreaProvider } from "react-native-safe-area-context";
import TabNavigator from "./TabNavigator";
import ActivityNavigator from "./ActivityNavigator";

export default function App({ signOut }) {
  return (
    <SafeAreaProvider>
      <NavigationContainer independent={true}>
        <TabNavigator signOut={signOut} />
      </NavigationContainer>
    </SafeAreaProvider>
  );
}
