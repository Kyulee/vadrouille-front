export default [
  {
    id: 1,
    name: "Zen",
    uri: require("../../assets/Icones/zenicon.png"),
    topPhoto:
      "https://www.clubvillamar.fr/content/lloret-de-mar/wp-content/uploads/2020/06/ontspannen-in-Lloret-de-Mar-mediteren-aan-het-strand-1.jpg",
    desc: "Et si je me relaxais aujourd'hui ?",
  },
  {
    id: 2,
    name: "Danse",
    uri: require("../../assets/Icones/fiestaicon.png"),
    topPhoto:
      "https://i0.wp.com/www.lille-danse.fr/wp-content/uploads/2022/06/ds8-Grande.jpeg?fit=1030%2C688&ssl=1",
    desc: "Je me dépense en dansant et en m'amusant !",
  },
  {
    id: 3,
    name: "Randonnée",
    uri: require("../../assets/Icones/hikingicon.png"),
    topPhoto:
      "https://www.aude.fr/sites/default/files/styles/768x430/public/media/images/4.%20Vivre%20dans%20l%27aude/4.4%20Nature/4.4.1%20Randonner/4.4.1%20MAjeure.jpg?itok=hWlfgYU1",
    desc: "Marcher n'a jamais été aussi simple",
  },
  {
    id: 4,
    name: "Sport de balle",
    uri: require("../../assets/Icones/ballsicon.png"),
    desc: "Ici, retrouve les activités de sport de balle",
  },
  {
    id: 5,
    name: "Artistique",
    uri: require("../../assets/Icones/articon.png"),
    desc: "Ici, retrouve les activités de sport de balle",
  },
  {
    id: 6,
    name: "Technologie",
    uri: require("../../assets/Icones/techicon.png"),
    desc: "Ici, retrouve les activités technologiques",
  },
  {
    id: 7,
    name: "Sport Aquatique",
    uri: require("../../assets/Icones/aquaicon.png"),
    desc: "Ici, retrouve les activités aquatiques",
  },
  {
    id: 8,
    name: "Cyclisme",
    uri: require("../../assets/Icones/bicycleicon.png"),
    desc: "Ici, retrouve les activités de cyclisme",
  },
  {
    id: 9,
    name: "Jeux",
    uri: require("../../assets/Icones/Union.png"),
    desc: "Ici, retrouve les activités ludiques",
  },
];
