export default [
    {
        id: 1,
        uri: require("../../assets/Icones/zenicon.png"),
    },
    {
        id: 2,
        uri: require("../../assets/Icones/fiestaicon.png"),
    },
    {
        id: 3,
        uri: require("../../assets/Icones/hikingicon.png"),
    },
    {
        id: 4,
        uri: require("../../assets/Icones/ballsicon.png"),
    },
    {
        id: 5,
        uri: require("../../assets/Icones/articon.png"),
    },
    {
        id: 6,
        uri: require("../../assets/Icones/techicon.png"),
    },
    {
        id: 7,
        uri: require("../../assets/Icones/aquaicon.png"),
    },
    {
        id: 8,
        uri: require("../../assets/Icones/bicycleicon.png"),
    },
    {
        id: 9,
        uri: require("../../assets/Icones/Union.png"),
    },
];
