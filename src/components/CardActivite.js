import React, { useState, useEffect, useMemo, useCallback } from "react";
import { useNavigation, useFocusEffect } from "@react-navigation/native";
import { Card, Icon } from "react-native-elements";
import { Text, View, ImageBackground, TouchableOpacity } from "react-native";
import { env } from "../../environment";
import storage from "../services/storage";

export const Activite = ({ activite }) => {
  const navigation = useNavigation();

  const [like, setLike] = useState(activite.isLiked);

  useEffect(() => {
    setLike(activite.isLiked);
  }, [activite]);

  return (
    <View style={{ marginBottom: 10 }} key={activite.id}>
      <TouchableOpacity
        onPress={() => {
          navigation.navigate("Activity", {
            name: activite.name,
            itemId: activite.id,
            activityDetails: activite,
          });
        }}
      >
        <ImageBackground
          source={{ uri: activite.uri }}
          style={{
            width: 350,
            height: 200,
            alignSelf: "center",
            overflow: "hidden",
            borderRadius: 10,
          }}
        >
          {like ? (
            <Icon
              name="heart"
              type="font-awesome"
              color="#f398bb"
              size={40}
              style={{
                alignSelf: "flex-end",
                marginRight: 10,
                marginTop: 10,
              }}
              onPress={() => {
                setLike(false);
                storage.remove({
                  key: "like",
                  id: activite.id,
                });
              }}
            />
          ) : (
            <Icon
              name="heart"
              type="font-awesome"
              color="#01B091"
              size={40}
              style={{
                alignSelf: "flex-end",
                marginRight: 10,
                marginTop: 10,
              }}
              onPress={() => {
                setLike(true);
                storage.save({
                  key: "like",
                  id: activite.id,
                  data: { ...activite, isLiked: true },
                });
              }}
            />
          )}
          <View
            style={{
              backgroundColor: "white",
              padding: 5,
              borderRadius: 10,
              margin: 10,
              width: 50,
              height: 40,
              bottom: 0,
              opacity: 10,
              justifyContent: "center",
              alignItems: "center",
              position: "absolute",
            }}
          >
            <Text style={{ fontSize: 20, color: "#01B091" }}>
              {activite.price} €
            </Text>
          </View>
        </ImageBackground>

        <View
          style={{
            flexDirection: "row",
            marginBottom: 20,
            marginTop: 20,
            marginHorizontal: 30,
            justifyContent: "space-evenly",
          }}
        >
          <Icon name="map-marker" type="font-awesome" color="#01B091" />
          <Text>{activite.distance} km</Text>
          <Icon name="star" type="font-awesome" color="#f9C62D" />
          <Text>{activite.rate}</Text>
          <Icon name="clock-o" type="font-awesome" color="#f23243" />
          <Text>{activite.duration} min</Text>
        </View>
        <Card.Title style={{ fontSize: 20 }}>{activite.name}</Card.Title>
        <Card.Divider style={{ width: 200, alignSelf: "center" }} />
      </TouchableOpacity>
    </View>
  );
};

const CardActivite = ({ category, setCount, search }) => {
  const [activities, setActivities] = useState([]);
  const memoized = useCallback(async () => {
    let url = env.apiUrl + `/activity`;
    if (search) url += `/search?activityName=` + search;
    else if (category) url += `/category/` + category;
    const res = await fetch(url, { method: search ? "POST" : "GET" });
    const json = await res.json();
    if (setCount) setCount(json.Count);
    return json.Items;
  }, [search]);

  useEffect(() => {
    (async () => {
      const activitiesLiked = await storage.getAllDataForKey("like");
      const acts = (await memoized()).map((act) => {
        return {
          ...act,
          isLiked: !!activitiesLiked.find((a) => a.id === act.id),
        };
      });
      setActivities([...acts]);
    })();
  }, [search]);

  useFocusEffect(
    useCallback(() => {
      const refreshLikes = async () => {
        const memoizedAct = await memoized();
        const activitiesLiked = await storage.getAllDataForKey("like");
        const acts = memoizedAct.map((act) => {
          return {
            ...act,
            isLiked: !!activitiesLiked.find((a) => a.id === act.id),
          };
        });
        setActivities(acts);
      };
      refreshLikes();
    }, [])
  );

  return (
    <View>
      {activities.map((activity) => (
        <Activite key={activity.id} activite={activity} />
      ))}
    </View>
  );
};

export default CardActivite;
