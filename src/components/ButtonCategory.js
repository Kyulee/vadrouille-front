import React from "react";
import { View, Text, Image, TouchableOpacity } from "react-native";
import { useNavigation } from "@react-navigation/native";

const ButtonCategory = ({ category, imageUri }) => {
  const navigation = useNavigation();
  return (
    <View>
      <View
        style={{
          backgroundColor: "#01B091",
          padding: 10,
          borderRadius: 10,
          margin: 10,
          width: 100,
          height: 100,
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <TouchableOpacity
          onPress={() => {
            navigation.navigate("Category", {
              category: category,
            });
          }}
        >
          <Image
            source={imageUri}
            style={{
              alignSelf: "center",
              marginBottom: 10,
            }}
          />
          <View />
          <Text style={{ color: "white", textAlign: "center" }}>
            {category.title}
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default ButtonCategory;
